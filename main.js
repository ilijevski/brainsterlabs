const navbarBtn = document.querySelector("#navi-toggle");
const navbar = document.querySelector(".navbar");
const navbarList = document.querySelector(".navbar__list");
const btn = document.querySelector(".btn");
const btnMarketing = document.querySelector("#marketing");
const btnProgramming = document.querySelector("#programming");
const btnDesign = document.querySelector("#design");
const cards = document.querySelectorAll(".card");
const cardsMarketing = document.querySelectorAll(".card-marketing");
const cardsProgramming = document.querySelectorAll(".card-programming");
const cardsDesign = document.querySelectorAll(".card-design");

//For the navbar burger menu
navbarBtn.addEventListener("click", () => {
  navbar.classList.toggle("active");
  navbarList.classList.toggle("active__list");
  btn.classList.toggle("active__btn");
});

//For the card display options
btnMarketing.addEventListener("click", () => {
  btnMarketing.classList.toggle("checked");
  if (btnMarketing.classList.contains("checked")) {
    btnProgramming.classList.remove("checked");
    btnDesign.classList.remove("checked");
    cards.forEach(function (card) {
      card.style.display = "none";
    });

    cardsMarketing.forEach(function (card) {
      card.style.display = "block";
    });
  } else {
    cards.forEach(function (card) {
      card.style.display = "block";
    });
  }
});

btnProgramming.addEventListener("click", () => {
  btnProgramming.classList.toggle("checked");
  if (btnProgramming.classList.contains("checked")) {
    btnMarketing.classList.remove("checked");
    btnDesign.classList.remove("checked");
    cards.forEach(function (card) {
      card.style.display = "none";
    });

    cardsProgramming.forEach(function (card) {
      card.style.display = "block";
    });
  } else {
    cards.forEach(function (card) {
      card.style.display = "block";
    });
  }
});

btnDesign.addEventListener("click", () => {
  btnDesign.classList.toggle("checked");
  if (btnDesign.classList.contains("checked")) {
    btnMarketing.classList.remove("checked");
    btnProgramming.classList.remove("checked");

    cards.forEach(function (card) {
      card.style.display = "none";
    });

    cardsDesign.forEach(function (card) {
      card.style.display = "block";
    });
  } else {
    cards.forEach(function (card) {
      card.style.display = "block";
    });
  }
});
